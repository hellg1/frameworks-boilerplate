from flask import request, jsonify, url_for
from app.models import Hardware
from app.api import bp
from app import db
from app.api.errors import bad_request

@bp.route('/hardware/<int:id>', methods=['GET'])
def get_harrrd(id):
    return jsonify(Hardware.query.get_or_404(id).to_dict())

@bp.route('/hardware', methods=['GET'])
def get_harrrds():
    # Какую страницу показать? По умолчанию 1
    page = request.args.get('page', 1, type=int)
    # Сколько элементов на странице? По умолчанию 10
    # Но не больше 100
    per_page = min(request.args.get('per_page', 10), 100)
    # Генерируем набор данных для страницы
    data = Hardware.to_collection_dict(Hardware.query, page, per_page, 'api.get_harrrds')
    # Возвращаем пользователю json
    return jsonify(data)

@bp.route('/hardware', methods=['POST'])
def create_hardware():
    # Проверяем, что запрос пришел с телом
    data = request.get_json() or {}
    # Если тела запроса нет, возвращаем ошибку
    if not data:
        return bad_request('Hardware should contain something')
    # Создаем экземпляр модели ORM для машины
    hardware = Hardware()
    # Загружаем данные из тела запроса в экземпляр
    hardware.from_dict(data)
    # Добавляем машину к текущей сессии с БД 
    db.session.add(hardware)
    # Комиттим транзакцию в БД
    db.session.commit()
    # По стандарту мы должны вернуть объект
    # с присвоенным уникальным идентификатором
    response = jsonify(hardware.to_dict())
    # Так же по стандарту код ответа должен быть 201 вместо 200
    # 200 OK
    # 201 Created
    response.status_code = 201
    # В заголовке передаем ссылку на созданный объект
    response.headers['Location'] = url_for('api.get_harrrds', id=hardware.id)
    return response

@bp.route('/hardware/<int:id>', methods=['PUT'])
def update_hardware(id):
    hardware = Hardware.query.get_or_404(id)
    data = request.get_json() or {}
    if not data:
        return bad_request('Build should contain something')
    hardware.from_dict(data)
    db.session.commit()
    return jsonify(hardware.to_dict())


@bp.route('/hardware/<int:id>', methods=['DELETE'])
def remove_hardware(id):
    hardware = Hardware.query.get_or_404(id)
    db.session.delete(hardware)
    db.session.commit()
    return jsonify(hardware.to_dict())
